{% macro ratings_classification_macro(column_name) %}
    CASE
        WHEN {{ column_name }} >= 4.5 THEN 'EXCELLENT'
        WHEN {{ column_name }} >= 4.0 THEN 'GOOD'
        WHEN {{ column_name }} >= 3.0 THEN 'AVERAGE'
        ELSE 'Poor'
    END
{% endmacro %}