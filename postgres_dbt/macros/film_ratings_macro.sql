{% macro generate_film_ratings() %}
-- depends_on: {{ ref('films') }}
with films_with_ratings as (
    select
    film_id,
    title,
    release_date,
    price,
    rating,
    user_rating,
    {{ ratings_classification_macro('user_rating') }}
    from {{ ref('films') }}
),

films_with_actors AS (
    SELECT
        f.film_id,
        f.title,
        STRING_AGG(a.actor_name, ', ') AS actors  -- Aggregating actor names for each film
    FROM {{ ref('films') }} f
    LEFT JOIN {{ ref('film_actors') }} fa ON f.film_id = fa.film_id
    LEFT JOIN {{ ref('actors') }} a ON fa.actor_id = a.actor_id
    GROUP BY f.film_id, f.title
)

SELECT
    fwf.*,
    fwa.actors
FROM films_with_ratings fwf
LEFT JOIN films_with_actors fwa ON fwf.film_id = fwa.film_id

{% endmacro %}
