import subprocess
import time

def wait_for_postgres(host,max_retries=5,delay_seconds=5):
    retries=0
    while retries<max_retries:
        try:
            result=subprocess.run(["pg_isready","-h",host],check=True,capture_output=True,text=True)
            if "accepting connections" in result.stdout:
                print("Source Database is Up and Running........")
                return True
        except subprocess.CalledProcessError as e:
            print(f"Error connecting to PostgreSQL: {e}")
            retries+=1
            print(f"Retrying in {delay_seconds} seconds... (Attempt {retries}/{max_retries})")
            time.sleep(delay_seconds)
    print("max retries reached. Exiting.")
    return False

if not wait_for_postgres(host="source_postgres"):
    exit(1)

print("Executing ELT script.......")

source_config = {
    'dbname': 'source_db',
    'user': 'postgres',
    'password': 'secret',
    'host': 'source_postgres'  # service name from docker-compose as the hostname for Source DB
}

destination_config = {
    'dbname': 'destination_db',
    'user': 'postgres',
    'password': 'secret',
    'host': 'destination_postgres' # Use the service name from docker-compose as the destination DB
}

dump_command = [
    'pg_dump',
    '-h', source_config['host'],
    '-U', source_config['user'],
    '-d', source_config['dbname'],
    '-f', 'data_dump.sql',
    '--no-password'  # Do not prompt for password
]
        


subprocess_env=dict(PGPASSWORD=source_config['password'])

subprocess.run(dump_command,env=subprocess_env,check=True)

load_command = [
    'psql',
    '-h', destination_config['host'],
    '-U', destination_config['user'],
    '-d', destination_config['dbname'],
    '-a', '-f', 'data_dump.sql'
]

subprocess_env=dict(PGPASSWORD=destination_config['password'])

subprocess.run(load_command,env=subprocess_env,check=True)

print("Completing ELT sccript......")


# # Run a command and capture its output
# result = subprocess.run(['ls', '-l'], stdout=subprocess.PIPE, text=False)


# # Print the output
# print(result)

# try:
#     result = subprocess.run(['ls', 'nonexistent_directory'], check=True, capture_output=True, text=True)
#     print(result.stdout)
# except subprocess.CalledProcessError as e:
#     print(f"Error: {e}")
#     print(f"Return Code: {e.returncode}")
#     print(f"Output (stderr): {e.stderr}")
# x=False
# print(not x)
# if not x:
#     print("x from inside")